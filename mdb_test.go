package mdb

import (
	"fmt"
	"os"
	"reflect"
	"testing"
	"time"

	"gitlab.com/go-mods/lib/mdb/cache"
)

func TestMdb_Open(t *testing.T) {
	throw(os.MkdirAll(".testdata/test", 0775))

	m := Mdb{}
	f, err := os.Create(".testdata/test/test2.db")
	throw(err)

	type args struct {
		path string
	}
	tests := []struct {
		name    string
		mdb     *Mdb
		args    args
		wantErr bool
	}{
		{
			name: "test.db",
			mdb:  &m,
			args: args{
				path: ".testdata/test/test.db",
			},
			wantErr: false,
		},
		{
			name: "test2.db",
			mdb:  &m,
			args: args{
				path: ".testdata/test/test2.db",
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.mdb.Open(tt.args.path); (err != nil) != tt.wantErr {
				t.Errorf("Mdb.Open() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}

	throw(f.Close())
	throw(os.RemoveAll(".testdata/test"))
	m.Close()
}

func TestMdb_Close(t *testing.T) {
	m := Mdb{}
	m2 := Mdb{}
	throw(m2.Open(".testdata/test/test.db"))

	tests := []struct {
		name string
		mdb  *Mdb
	}{
		{
			name: "closed",
			mdb:  &m,
		},
		{
			name: "opened",
			mdb:  &m2,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.mdb.Close()
		})
	}

	throw(os.RemoveAll(".testdata/test"))
}

func TestMdb_Con(t *testing.T) {
	m := Mdb{Cache: cache.Cache{Duration: time.Minute}}
	throw(m.Open(".testdata/test/test.db"))
	m.Add("a", []byte{1})
	m.Add("b", []byte{2})

	type args struct {
		key string
	}
	tests := []struct {
		name string
		mdb  *Mdb
		args args
		want bool
	}{
		{
			name: "\"a\"",
			mdb:  &m,
			args: args{
				key: "a",
			},
			want: true,
		},
		{
			name: "\"b\"",
			mdb:  &m,
			args: args{
				key: "b",
			},
			want: true,
		},
		{
			name: "\"c\"",
			mdb:  &m,
			args: args{
				key: "c",
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.mdb.Con(tt.args.key); got != tt.want {
				t.Errorf("Mdb.Con() = %v, want %v", got, tt.want)
			}
		})
	}

	m.Close()
	throw(os.RemoveAll(".testdata/test"))
}

func TestMdb_Get(t *testing.T) {
	m := Mdb{Cache: cache.Cache{Duration: time.Minute}}
	throw(m.Open(".testdata/test/test.db"))
	m.Add("a", []byte{1, 2, 3})
	m.Add("b", []byte{4, 5, 6})

	m2 := Mdb{Cache: cache.Cache{Duration: time.Millisecond}}
	throw(m2.Open(".testdata/test/test2.db"))
	m2.Add("c", []byte{7, 8, 9})
	<-time.After(2 * time.Millisecond)

	type args struct {
		key string
	}
	tests := []struct {
		name  string
		mdb   *Mdb
		args  args
		want  []byte
		want1 bool
	}{
		{
			name: "\"a\"",
			mdb:  &m,
			args: args{
				key: "a",
			},
			want:  []byte{1, 2, 3},
			want1: true,
		},
		{
			name: "\"b\"",
			mdb:  &m,
			args: args{
				key: "b",
			},
			want:  []byte{4, 5, 6},
			want1: true,
		},
		{
			name: "\"c\"",
			mdb:  &m2,
			args: args{
				key: "c",
			},
			want:  []byte{7, 8, 9},
			want1: true,
		},
		{
			name: "\"d\"",
			mdb:  &m2,
			args: args{
				key: "d",
			},
			want:  nil,
			want1: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1 := tt.mdb.Get(tt.args.key)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Mdb.Get() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("Mdb.Get() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}

	m.Close()
	m2.Close()
	throw(os.RemoveAll(".testdata/test"))
}

func TestMdb_Add(t *testing.T) {
	m := Mdb{}
	throw(m.Open(".testdata/test/test.db"))

	type args struct {
		key   string
		value []byte
	}
	tests := []struct {
		name string
		mdb  *Mdb
		args args
	}{
		{
			name: "\"a\",()",
			mdb:  &m,
			args: args{
				key:   "a",
				value: []byte{},
			},
		},
		{
			name: "\"b\",(1)",
			mdb:  &m,
			args: args{
				key:   "b",
				value: []byte{1},
			},
		},
		{
			name: "\"c\",(1,2)",
			mdb:  &m,
			args: args{
				key:   "c",
				value: []byte{1, 2},
			},
		},
		{
			name: "\"c\",(1,2,3)",
			mdb:  &m,
			args: args{
				key:   "c",
				value: []byte{1, 2, 3},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.mdb.Add(tt.args.key, tt.args.value)
		})
	}

	m.Close()
	throw(os.RemoveAll(".testdata/test"))
}

func TestMdb_Del(t *testing.T) {
	m := Mdb{}
	throw(m.Open(".testdata/test/test.db"))
	m.Add("a", []byte{1})
	m.Add("b", []byte{2})
	m.Add("c", []byte{3})

	type args struct {
		key string
	}
	tests := []struct {
		name string
		mdb  *Mdb
		args args
		want bool
	}{
		{
			name: "\"a\"",
			mdb:  &m,
			args: args{
				key: "a",
			},
			want: true,
		},
		{
			name: "\"b\"",
			mdb:  &m,
			args: args{
				key: "b",
			},
			want: true,
		},
		{
			name: "\"c\"",
			mdb:  &m,
			args: args{
				key: "c",
			},
			want: true,
		},
		{
			name: "\"c\"",
			mdb:  &m,
			args: args{
				key: "c",
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.mdb.Del(tt.args.key); got != tt.want {
				t.Errorf("Mdb.Del() = %v, want %v", got, tt.want)
			}
		})
	}

	m.Close()
	throw(os.RemoveAll(".testdata/test"))
}

func TestMdb_String(t *testing.T) {
	m := Mdb{}
	throw(m.Open(".testdata/test/test.db"))
	m.Add("a", []byte{1})
	s := fmt.Sprintf("%v", m.handles)

	tests := []struct {
		name string
		mdb  Mdb
		want string
	}{
		{
			name: "()",
			mdb:  m,
			want: s,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.mdb.String(); got != tt.want {
				t.Errorf("Mdb.String() = %v, want %v", got, tt.want)
			}
		})
	}

	m.Close()
	throw(os.RemoveAll(".testdata/test"))
}
