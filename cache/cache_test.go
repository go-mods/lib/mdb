package cache

import (
	"fmt"
	"reflect"
	"testing"
	"time"
)

func TestCache_Init(t *testing.T) {
	c := Cache{}

	tests := []struct {
		name string
		c    *Cache
	}{
		{
			name: "()",
			c:    &c,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.c.Init()
		})
	}
}

func TestCache_Get(t *testing.T) {
	c := Cache{Duration: time.Minute}
	c.Init()

	c.Add("a", []byte{1})
	c.Add("b", []byte{2})

	c2 := Cache{Duration: time.Millisecond}
	c2.Init()

	c2.Add("c", []byte{3})
	<-time.After(2 * time.Millisecond)

	type args struct {
		key string
	}
	tests := []struct {
		name  string
		c     *Cache
		args  args
		want  []byte
		want1 bool
	}{
		{
			name: "\"a\"",
			c:    &c,
			args: args{
				key: "a",
			},
			want:  []byte{1},
			want1: true,
		},
		{
			name: "\"b\"",
			c:    &c,
			args: args{
				key: "b",
			},
			want:  []byte{2},
			want1: true,
		},
		{
			name: "\"c\"",
			c:    &c2,
			args: args{
				key: "c",
			},
			want:  nil,
			want1: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1 := tt.c.Get(tt.args.key)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Cache.Get() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("Cache.Get() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

func TestCache_Del(t *testing.T) {
	c := Cache{Duration: time.Minute}
	c.Init()

	c.Add("a", []byte{1})
	c.Add("b", []byte{2})

	type args struct {
		key string
	}
	tests := []struct {
		name string
		c    *Cache
		args args
		want bool
	}{
		{
			name: "\"a\"",
			c:    &c,
			args: args{
				key: "a",
			},
			want: true,
		},
		{
			name: "\"b\"",
			c:    &c,
			args: args{
				key: "b",
			},
			want: true,
		},
		{
			name: "\"c\"",
			c:    &c,
			args: args{
				key: "c",
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.c.Del(tt.args.key); got != tt.want {
				t.Errorf("Cache.Del() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCache_Add(t *testing.T) {
	c := Cache{Duration: time.Minute}
	c.Init()

	type args struct {
		key  string
		data []byte
	}
	tests := []struct {
		name string
		c    *Cache
		args args
	}{
		{
			name: "\"a\",1",
			c:    &c,
			args: args{
				key:  "a",
				data: []byte{1},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.c.Add(tt.args.key, tt.args.data)
		})
	}
}

func TestCache_String(t *testing.T) {
	c := Cache{Duration: time.Minute}
	c.Init()

	c.Add("a", []byte{1})
	c.Add("b", []byte{2})

	tests := []struct {
		name string
		c    *Cache
		want string
	}{
		{
			name: "()",
			c:    &c,
			want: fmt.Sprintf("%v", map[string][]byte{
				"a": []byte{1},
				"b": []byte{2},
			}),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.c.String(); got != tt.want {
				t.Errorf("Cache.String() = %v, want %v", got, tt.want)
			}
		})
	}
}
