module gitlab.com/go-mods/lib/mdb

go 1.17

require (
	gitlab.com/go-mods/lib/bops v0.0.0-20220410201211-aa39c5a39a7c
	gitlab.com/go-mods/lib/hscd v0.0.0-20220410202125-110e3cfd8c65
)
